﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class VideoPlayerInterface : MonoBehaviour
{

    public VideoPlayer m_videoPlayer;
    [Range(0, 1f)]
    public float m_musicTimer;
    public OnTimeChangeEvent m_onTimeChanged;
    [System.Serializable]
    public class OnTimeChangeEvent : UnityEvent <float>{ }


    [Header("Debug")]
    [Range(0, 1f)]
    public float m_musicTimerViewer;
    public double m_musicTime;

    public void Update()
    {
        m_musicTimerViewer = (float)((double)m_videoPlayer.frame / (double)m_videoPlayer.frameCount);
        m_musicTime = m_videoPlayer.time;
    }
    public void OnValidate()
    {
        SetMusicTimeTo(m_musicTimer );
    }

    public void SetMusicTimeTo(float timePourcent)
    {

        timePourcent = Mathf.Clamp(timePourcent, 0, 0.999999f);
        m_videoPlayer.frame = (int)(m_videoPlayer.frameCount * timePourcent);
        RefreshCursorTime(timePourcent);


    }

    private void RefreshCursorTime(float timePourcent)
    {
        //m_musicTimer = timePourcent;
        m_onTimeChanged.Invoke(timePourcent);
    }

    public void ResetToZero()
    {
        m_videoPlayer.frame = 0;
    }
    public void Play()
    {
        m_videoPlayer.Play();
    }
    public void Pause() {
        m_videoPlayer.Pause();
    }

    public void BackInTime(float time) {
        double newTime = m_videoPlayer.time -time;
        if (newTime < 0f) newTime = 0;
        m_videoPlayer.time = newTime;


    }
}
