﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Linq;

[CustomEditor(typeof(BipOnTune))]
[CanEditMultipleObjects]
public class BipOnTuneEditor : Editor
{

    public static string m_lookFor;
    public override void OnInspectorGUI()
    {
        BipOnTune tune = (BipOnTune)target;

        DrawDefaultInspector();
        EditorGUILayout.BeginHorizontal();
        m_lookFor = EditorGUILayout.TextField(m_lookFor);
        if (GUILayout.Button("Add all"))
        {
            BipToDo[] allToDoInScene = GameObject.FindObjectsOfType<BipToDo>();
            Debug.Log("To Do " + allToDoInScene.Length);

            foreach (BipToDo todo in allToDoInScene)
            {
                if (todo.m_keyFrame.m_descriptionName == m_lookFor)
                {
                    tune.AddKeyFrame(todo.m_keyFrame);
                }
            }
        }


        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Clear"))
        {
            tune.Clear();
        }
        if (GUILayout.Button("Sort"))
        {
            tune.m_track.m_keyframes=  tune.m_track.m_keyframes.OrderBy(k => k.m_time).ToList<TuneKeyFrame>();
        }


    }
}